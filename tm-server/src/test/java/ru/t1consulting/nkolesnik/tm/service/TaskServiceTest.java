package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(DataCategory.class)
public class TaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @Nullable
    private static final String NULL_TASK_NAME = null;

    @Nullable
    private static final String NULL_TASK_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final Integer NULL_TASK_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_TASK_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Task NULL_TASK = null;

    private static final long REPOSITORY_SIZE = 10L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private final String taskId = UUID.randomUUID().toString();

    @NotNull
    private List<Task> tasks;

    @NotNull
    private Task task;

    @BeforeClass
    public static void prepareTestEnvironment() {
        createDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        restoreDbBackup();
    }

    @SneakyThrows
    private static void createDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlSaveData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveData);
            statement.executeUpdate(sqlClearTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    private static void restoreDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlClearTable = "DELETE FROM \"tasks\";";
        String sqlLoadData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        String sqlDropBackupTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTable);
            statement.executeUpdate(sqlLoadData);
            statement.executeUpdate(sqlDropBackupTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        task = createOneTask();
        tasks = createManyTasks();
    }

    @After
    public void cleanup() {
        taskService.clear();
    }

    @Test
    public void add() {
        taskService.add(task);
        Assert.assertEquals(1, taskService.getSize());
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE + 1, taskService.getSize());
    }

    @Test
    public void set() {
        taskService.set(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void existById() {
        taskService.add(task);
        Assert.assertFalse(taskService.existsById(NULL_TASK_ID));
        Assert.assertFalse(taskService.existsById(EMPTY_TASK_ID));
        Assert.assertTrue(taskService.existsById(taskId));
    }

    @Test
    public void findAll() {
        taskService.add(tasks);
        Assert.assertEquals(tasks.size(), taskService.findAll().size());
    }

    @Test
    public void findById() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findById(NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findById(EMPTY_TASK_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findById(UUID.randomUUID().toString()));
        @Nullable final Task repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void findByIndex() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(-1));
        @Nullable final Task repositoryTask = taskService.findByIndex(0);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void remove() {
        taskService.add(task);
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.remove(NULL_TASK));
        @Nullable final Task repositoryTask = taskService.remove(task);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void removeById() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(EMPTY_TASK_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeById(UUID.randomUUID().toString()));
        @Nullable final Task repositoryTask = taskService.removeById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void removeByIndex() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(-1));
        @Nullable final Task repositoryTask = taskService.removeByIndex(0);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void removeAll() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
        taskService.removeAll(tasks);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void clear() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
        taskService.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void getSize() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void addWithUserId() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(EMPTY_USER_ID, task));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.add(userId, NULL_TASK));
    }

    @Test
    public void existByIdWithUserId() {
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(userId, NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(userId, EMPTY_TASK_ID));
    }

    @Test
    public void findAllWithUserId() {
        for (Task task : tasks)
            taskService.add(userId, task);
        taskService.add(userId, task);
        Assert.assertEquals(REPOSITORY_SIZE + 1, taskService.findAll(userId).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void findByIdWithUserId() {
        for (Task task : tasks)
            taskService.add(userId, task);
        taskService.add(userId, task);
        @Nullable final Task repositoryTask = taskService.findById(userId, taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findById(userId, NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findById(userId, EMPTY_TASK_ID));
    }

    @Test
    public void findByIndexWithUserId() {
        taskService.add(userId, task);
        for (Task task : tasks)
            taskService.add(userId, task);
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(userId, NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(userId, 10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(userId, -1));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findByIndex(EMPTY_USER_ID, 0));
        @Nullable final Task repositoryTask = taskService.findByIndex(userId, 0);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void removeWithUserId() {
        taskService.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(EMPTY_USER_ID, task));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.remove(userId, NULL_TASK));
        @Nullable final Task repositoryTask = taskService.remove(userId, task);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void removeByIdWithUserId() {
        taskService.add(userId, task);
        for (Task task : tasks)
            taskService.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(userId, NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(userId, EMPTY_TASK_ID));
        Assert.assertThrows(
                ModelNotFoundException.class,
                () -> taskService.removeById(userId, UUID.randomUUID().toString())
        );
        @Nullable final Task repositoryTask = taskService.removeById(userId, taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void removeByIndexWithUserId() {
        taskService.add(task);
        for (Task task : tasks)
            taskService.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(userId, NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(-1));
        @Nullable final Task repositoryTask = taskService.removeByIndex(userId, 0);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void clearWithUserId() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(EMPTY_USER_ID));
        taskService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(EMPTY_USER_ID));
        taskService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void create() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.create(NULL_USER_ID, TASK_NAME_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(userId, NULL_TASK_NAME));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.create(userId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void updateById() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> taskService.updateById(NULL_USER_ID, taskId, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> taskService.updateById(userId, NULL_TASK_ID, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateById(userId, taskId, NULL_TASK_NAME, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateById(userId, taskId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void updateByIndex() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> taskService.updateByIndex(NULL_USER_ID, 0, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> taskService.updateByIndex(userId, NULL_TASK_INDEX, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateByIndex(userId, 0, NULL_TASK_NAME, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateByIndex(userId, 0, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void changeStatusById() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> taskService.changeTaskStatusById(NULL_USER_ID, taskId, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> taskService.changeTaskStatusById(userId, NULL_TASK_ID, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> taskService.changeTaskStatusById(userId, taskId, NULL_STATUS)
        );
    }

    @Test
    public void changeStatusByIndex() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> taskService.changeTaskStatusByIndex(NULL_USER_ID, 0, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> taskService.changeTaskStatusByIndex(userId, NULL_TASK_INDEX, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> taskService.changeTaskStatusByIndex(userId, 0, NULL_STATUS)
        );
    }

    @Test
    public void findAllByProjectId() {
        for (Task task : tasks)
            taskService.add(userId, task);
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.findAllByProjectId(NULL_USER_ID, taskId));
        Assert.assertEquals(REPOSITORY_SIZE, taskService.findAllByProjectId(userId, projectId).size());
    }

    @NotNull
    private Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }

}
