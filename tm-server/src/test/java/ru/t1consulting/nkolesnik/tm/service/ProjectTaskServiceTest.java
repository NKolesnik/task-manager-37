package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.sql.Connection;
import java.sql.Statement;
import java.util.UUID;

@Category(DataCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private final String taskId = UUID.randomUUID().toString();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private Task task;

    @NotNull
    private Project project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        createDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        restoreDbBackup();
    }

    @SneakyThrows
    private static void createDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        String sqlClearProjectTable = "DELETE FROM \"projects\";";
        String sqlSavTaskeData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlSavTaskeData);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlClearTaskTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    private static void restoreDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlClearProjectTable = "DELETE FROM \"projects\";";
        String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupProjectTable);
            statement.executeUpdate(sqlDropBackupTaskTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        task = createOneTask();
        project = createOneProject();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
    }

    @Test
    public void bindTaskToProject() {
        taskService.add(userId, task);
        projectService.add(userId, project);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(NULL_USER_ID, taskId, projectId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, NULL_PROJECT_ID, taskId)
        );
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @Nullable final Task repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(projectId, repositoryTask.getProjectId());
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

}
