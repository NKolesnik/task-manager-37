package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

@Category(DataCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @Nullable
    private static final Integer NULL_PROJECT_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_PROJECT_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Project NULL_PROJECT = null;

    @Nullable
    private static final Sort NULL_SORT = null;

    private static final long REPOSITORY_SIZE = 1000L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectRepository repository = new ProjectRepository(connectionService.getConnection());

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projects;

    @NotNull
    private Project project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        createDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        restoreDbBackup();
    }

    @SneakyThrows
    private static void createDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlSaveData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        String sqlClearTable = "DELETE FROM \"projects\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveData);
            statement.executeUpdate(sqlClearTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    private static void restoreDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlClearTable = "DELETE FROM \"projects\";";
        String sqlLoadData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        String sqlDropBackupTable = "DROP TABLE \"backup_projects\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTable);
            statement.executeUpdate(sqlLoadData);
            statement.executeUpdate(sqlDropBackupTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        project = createOneProject();
        projects = createManyProjects();
    }

    @After
    public void cleanup() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void addAll() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void set() {
        repository.set(projects);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void existById() {
        repository.add(projects);
        @Nullable final Project repositoryProject = repository.findByIndex(100);
        Assert.assertNotNull(repositoryProject);
        @NotNull final String id = repositoryProject.getId();
        Assert.assertTrue(repository.existsById(id));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        Assert.assertNotNull(repository.findAll());
        repository.add(projects);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll().size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findAllWithSort() {
        repository.add(projects);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(Sort.BY_STATUS).size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findAllWithComporator() {
        repository.add(projects);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(Sort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findByIndex() {
        repository.add(project);
        @Nullable final Project repositoryProject = repository.findByIndex(0);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertNull(repository.findByIndex(1));
        Assert.assertNull(repository.findByIndex(-1));
    }

    @Test
    public void findById() {
        repository.add(project);
        @Nullable final Project repositoryProject = repository.findById(projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertNull(repository.findById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findById(NULL_PROJECT_ID));
    }

    @Test
    public void remove() {
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNotNull(repository.remove(project));
        Assert.assertNull(repository.findById(projectId));
    }

    @Test
    public void removeById() {
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
        repository.removeById(projectId);
        Assert.assertNull(repository.findById(projectId));
    }

    @Test
    public void removeByIndex() {
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNotNull(repository.removeByIndex(0));
        Assert.assertNull(repository.removeByIndex(1));
        Assert.assertNull(repository.findById(projectId));
    }

    @Test
    public void removeAll() {
        repository.add(projects);
        Assert.assertEquals(projects.size(), repository.getSize());
        repository.removeAll(projects);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void clear() {
        repository.add(projects);
        Assert.assertEquals(projects.size(), repository.getSize());
        repository.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void getSize() {
        final long projectsSize = projects.size();
        repository.add(projects);
        Assert.assertEquals(projectsSize, repository.getSize());
    }

    @Test
    public void addByUserId() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(userId, project);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void existByIdByUserId() {
        repository.add(projects);
        @Nullable final Project repositoryProject = repository.findByIndex(100);
        Assert.assertNotNull(repositoryProject);
        @NotNull final String id = repositoryProject.getId();
        Assert.assertTrue(repository.existsById(userId, id));
        Assert.assertFalse(repository.existsById(userId, EMPTY_PROJECT_ID));
    }

    @Test
    public void findAllByUserId() {
        repository.add(projects);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(userId).size();
        Assert.assertEquals(repositorySize, findSize);
        Assert.assertNotNull(repository.findAll(UUID.randomUUID().toString()));
        Assert.assertNotNull(repository.findAll(NULL_USER_ID));
    }

    @Test
    public void findAllWithSortByUserId() {
        repository.add(projects);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(userId, Sort.BY_STATUS).size();
        Assert.assertNotNull(repository.findAll(NULL_USER_ID));
        Assert.assertNotNull(repository.findAll(userId, NULL_SORT));
        Assert.assertEquals(repositorySize, findSize);
        Assert.assertNotNull(repository.findAll(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllWithComparatorByUserId() {
        repository.add(projects);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(userId, Sort.BY_STATUS.getComparator()).size();
        Assert.assertNotNull(repository.findAll(NULL_USER_ID));
        Assert.assertNotNull(repository.findAll(userId, NULL_SORT));
        Assert.assertEquals(repositorySize, findSize);
        Assert.assertNotNull(repository.findAll(UUID.randomUUID().toString()));
    }

    @Test
    public void findByIndexByUserId() {
        repository.add(project);
        @Nullable final Project repositoryProject = repository.findByIndex(userId, 0);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertNull(repository.findByIndex(NULL_USER_ID, 1));
        Assert.assertNull(repository.findByIndex(EMPTY_USER_ID, 1));
        Assert.assertNull(repository.findByIndex(userId, NULL_PROJECT_INDEX));
        Assert.assertNull(repository.findByIndex(userId, 1));
        Assert.assertNull(repository.findByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void findByIdByUserId() {
        repository.add(project);
        @Nullable final Project repositoryProject = repository.findById(userId, project.getId());
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertNull(repository.findById(NULL_USER_ID, NULL_PROJECT_ID));
        Assert.assertNull(repository.findById(userId, NULL_PROJECT_ID));
        Assert.assertNull(repository.findById(EMPTY_USER_ID, project.getId()));
        Assert.assertNull(repository.findById(EMPTY_USER_ID, EMPTY_PROJECT_ID));
        Assert.assertNull(repository.findById(userId, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertNull(repository.findById(NULL_USER_ID, project.getId()));
    }

    @Test
    public void removeByUserId() {
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.remove(NULL_USER_ID, project));
        Assert.assertNull(repository.remove(EMPTY_USER_ID, project));
        Assert.assertNull(repository.remove(userId, NULL_PROJECT));
        Assert.assertNotNull(repository.remove(userId, project));
        Assert.assertNull(repository.findById(userId, project.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.removeById(NULL_USER_ID, project.getId()));
        Assert.assertNull(repository.removeById(EMPTY_USER_ID, project.getId()));
        Assert.assertNull(repository.removeById(userId, NULL_PROJECT_ID));
        Assert.assertNull(repository.removeById(userId, EMPTY_PROJECT_ID));
        Assert.assertNotNull(repository.removeById(userId, project.getId()));
        Assert.assertNull(repository.findById(userId, project.getId()));
    }

    @Test
    public void removeByIndexByUserId() {
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.removeByIndex(NULL_USER_ID, 0));
        Assert.assertNull(repository.removeByIndex(EMPTY_USER_ID, 0));
        Assert.assertNull(repository.removeByIndex(userId, NULL_PROJECT_INDEX));
        Assert.assertNull(repository.removeByIndex(userId, -1));
        Assert.assertNull(repository.removeByIndex(userId, 1));
        Assert.assertNotNull(repository.removeByIndex(project.getUserId(), 0));
        Assert.assertNull(repository.findById(userId, project.getId()));
    }

    @Test
    public void clearByUserId() {
        repository.add(projects);
        Assert.assertEquals(projects.size(), repository.getSize());
        repository.clear(userId);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void getSizeByUserId() {
        repository.add(userId, project);
        Assert.assertEquals(1, repository.getSize(userId));
        repository.removeByIndex(userId, 0);
        Assert.assertEquals(0, repository.getSize(userId));
    }

    @Test
    public void createWithUserIdAndTaskName() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.create(userId, PROJECT_NAME_PREFIX);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void createWithUserIdAndTaskNameAndTaskDescription() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.create(userId, PROJECT_NAME_PREFIX, PROJECT_DESCRIPTION_PREFIX);
        Assert.assertEquals(1, repository.getSize());
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private List<Project> createManyProjects() {
        @NotNull final List<Project> projects = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Project project = new Project();
            project.setName(PROJECT_NAME_PREFIX + i);
            project.setDescription(PROJECT_DESCRIPTION_PREFIX + i);
            project.setUserId(userId);
            projects.add(project);
        }
        return projects;
    }

}