package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public static final String TABLE_NAME = "\"users\"";

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull ResultSet resultSet) {
        @NotNull final User user = new User();
        user.setId(resultSet.getString("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPasswordHash(resultSet.getString("password_hash"));
        user.setEmail(resultSet.getString("email"));
        user.setFirstName(resultSet.getString("fst_name"));
        user.setMiddleName(resultSet.getString("mid_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setRole(Role.toRole(resultSet.getString("role")));
        user.setLocked(resultSet.getBoolean("locked_flg"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sqlCommand = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) " +
                        "VALUES (?,?,?,?,?,?,?,?,?)",
                getTableName(),
                formatColumnName("id"),
                formatColumnName("login"),
                formatColumnName("password_hash"),
                formatColumnName("email"),
                formatColumnName("fst_name"),
                formatColumnName("mid_name"),
                formatColumnName("last_name"),
                formatColumnName("role"),
                formatColumnName("locked_flg")
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, user.getId());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPasswordHash());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getFirstName());
            preparedStatement.setString(6, user.getMiddleName());
            preparedStatement.setString(7, user.getLastName());
            preparedStatement.setString(8, user.getRole().name());
            preparedStatement.setBoolean(9, user.getLocked());
            preparedStatement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"login\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, login);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"email\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, email);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@NotNull final String login) {
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"login\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, login);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@NotNull final String email) {
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"email\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, email);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull User user) {
        @NotNull final String sqlCommand = String.format(
                "UPDATE %s " +
                        "SET %s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?" +
                        "WHERE \"id\" = ?;"
                , getTableName(),
                formatColumnName("login"),
                formatColumnName("password_hash"),
                formatColumnName("email"),
                formatColumnName("fst_name"),
                formatColumnName("mid_name"),
                formatColumnName("last_name"),
                formatColumnName("role"),
                formatColumnName("locked_flg")
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPasswordHash());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getFirstName());
            preparedStatement.setString(5, user.getMiddleName());
            preparedStatement.setString(6, user.getLastName());
            preparedStatement.setString(7, user.getRole().getDisplayName());
            preparedStatement.setBoolean(8, user.getLocked());
            preparedStatement.setString(9, user.getId());
            preparedStatement.executeUpdate();
            return user;
        }
    }

}
