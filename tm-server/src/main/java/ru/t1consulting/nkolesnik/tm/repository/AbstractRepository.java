package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.IRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import java.sql.*;
import java.util.Date;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet resultSet);

    @NotNull
    protected abstract String getTableName();

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> result = new ArrayList<>();
        models.forEach(model -> result.add(add(model)));
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> models = new ArrayList<>();
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + ";";
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sqlCommand);
            while (resultSet.next()) {
                models.add(fetch(resultSet));
            }
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> models = new ArrayList<>();
        @NotNull final String comparatorName = comparator.toString();
        @NotNull final String sqlCommand = "SELECT *" +
                " FROM " + getTableName() +
                " ORDER BY " + formatColumnName(comparatorName) +
                ";";
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sqlCommand);
            while (resultSet.next()) {
                models.add(fetch(resultSet));
            }
        }
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index > getSize()) return null;
        @NotNull final String sqlCommand = "SELECT *" +
                " FROM " + getTableName() +
                " LIMIT 1" +
                " OFFSET ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setInt(1, index);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final String sqlCommand = "SELECT *" +
                " FROM " + getTableName() +
                " WHERE " + formatColumnName("id") + " = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, id);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final String id = model.getId();
        return removeById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) {
        @Nullable final M model = findById(id);
        if (model == null) return null;
        @NotNull final String sqlCommand = "DELETE" +
                " FROM " + getTableName() +
                " WHERE " + formatColumnName("id") + " = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
            return model;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final Integer index) {
        @Nullable final M model = findByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collections) {
        collections.forEach(this::remove);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sqlCommand = "DELETE" +
                " FROM " + getTableName() + ";";
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlCommand);
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final String sqlCommand = "SELECT COUNT(1) as \"CNT\"" +
                " FROM " + getTableName() + ";";
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sqlCommand);
            if (resultSet.next()) {
                return resultSet.getLong("CNT");
            }
            return 0;
        }
    }

    protected String formatColumnName(@Nullable final String columnName) {
        if (columnName == null || columnName.isEmpty()) return "";
        return "\"" + columnName + "\"";
    }

    @Nullable
    protected Timestamp convertJavaDateToSqlDate(@Nullable final Date value) {
        if (value == null) return null;
        return new Timestamp(value.getTime());
    }

}
