package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    private static final String TABLE_NAME = "\"tasks\"";

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String sqlCommand = "SELECT * FROM " +
                getTableName() +
                " WHERE " +
                formatColumnName("user_id") +
                " = ? AND " +
                formatColumnName("project_id") +
                " = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, projectId);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                tasks.add(fetch(resultSet));
            }
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull ResultSet resultSet) {
        @NotNull final Task task = new Task();
        task.setId(resultSet.getString("id"));
        task.setUserId(resultSet.getString("user_id"));
        task.setName(resultSet.getString("name"));
        task.setDescription(resultSet.getString("description"));
        task.setProjectId(resultSet.getString("project_id"));
        task.setStatus(Status.toStatus(resultSet.getString("status")));
        task.setCreated(resultSet.getDate("created"));
        task.setDateBegin(resultSet.getDate("date_begin"));
        task.setDateEnd(resultSet.getDate("date_end"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sqlCommand =
                String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?,?,?)",
                        getTableName(),
                        formatColumnName("id"),
                        formatColumnName("user_id"),
                        formatColumnName("name"),
                        formatColumnName("description"),
                        formatColumnName("project_id"),
                        formatColumnName("status"),
                        formatColumnName("created"),
                        formatColumnName("date_begin"),
                        formatColumnName("date_end")
                );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, task.getId());
            preparedStatement.setString(2, task.getUserId());
            preparedStatement.setString(3, task.getName());
            preparedStatement.setString(4, task.getDescription());
            preparedStatement.setString(5, task.getProjectId());
            preparedStatement.setString(6, task.getStatus().toString());
            preparedStatement.setTimestamp(7, convertJavaDateToSqlDate(task.getCreated()));
            preparedStatement.setTimestamp(8, convertJavaDateToSqlDate(task.getDateBegin()));
            preparedStatement.setTimestamp(9, convertJavaDateToSqlDate(task.getDateEnd()));
            preparedStatement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull Task task) {
        @NotNull final String sqlCommand = String.format(
                "UPDATE %s " +
                        "SET %s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?" +
                        "WHERE \"id\" = ?;"
                , getTableName(),
                formatColumnName("user_id"),
                formatColumnName("name"),
                formatColumnName("description"),
                formatColumnName("project_id"),
                formatColumnName("status"),
                formatColumnName("created"),
                formatColumnName("date_begin"),
                formatColumnName("date_end")
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, task.getUserId());
            preparedStatement.setString(2, task.getName());
            preparedStatement.setString(3, task.getDescription());
            preparedStatement.setString(4, task.getProjectId());
            preparedStatement.setString(5, task.getStatus().toString());
            preparedStatement.setTimestamp(6, convertJavaDateToSqlDate(task.getCreated()));
            preparedStatement.setTimestamp(7, convertJavaDateToSqlDate(task.getDateBegin()));
            preparedStatement.setTimestamp(8, convertJavaDateToSqlDate(task.getDateEnd()));
            preparedStatement.setString(9, task.getId());
            preparedStatement.executeUpdate();
            return task;
        }
    }

}
