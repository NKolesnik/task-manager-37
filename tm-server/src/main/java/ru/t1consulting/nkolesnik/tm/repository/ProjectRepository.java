package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public static final String TABLE_NAME = "\"projects\"";

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sqlCommand = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?)",
                getTableName(),
                formatColumnName("id"),
                formatColumnName("user_id"),
                formatColumnName("name"),
                formatColumnName("description"),
                formatColumnName("status"),
                formatColumnName("date_begin"),
                formatColumnName("date_end")
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, project.getId());
            preparedStatement.setString(2, project.getUserId());
            preparedStatement.setString(3, project.getName());
            preparedStatement.setString(4, project.getDescription());
            preparedStatement.setString(5, project.getStatus().toString());
            preparedStatement.setTimestamp(6, convertJavaDateToSqlDate(project.getDateBegin()));
            preparedStatement.setTimestamp(7, convertJavaDateToSqlDate(project.getDateEnd()));
            preparedStatement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    @SneakyThrows
    protected @NotNull Project fetch(@NotNull ResultSet resultSet) {
        @NotNull final Project project = new Project();
        project.setId(resultSet.getString("id"));
        project.setUserId(resultSet.getString("user_id"));
        project.setName(resultSet.getString("name"));
        project.setDescription(resultSet.getString("description"));
        project.setStatus(Status.toStatus(resultSet.getString("status")));
        project.setDateBegin(resultSet.getTimestamp("date_begin"));
        project.setDateEnd(resultSet.getTimestamp("date_end"));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull Project project) {
        @NotNull final String sqlCommand = String.format(
                "UPDATE %s " +
                        "SET %s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "%s = ?," +
                        "WHERE \"id\" = ?;",
                getTableName(),
                formatColumnName("user_id"),
                formatColumnName("name"),
                formatColumnName("description"),
                formatColumnName("status"),
                formatColumnName("date_begin"),
                formatColumnName("date_end")
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, project.getUserId());
            preparedStatement.setString(2, project.getName());
            preparedStatement.setString(3, project.getDescription());
            preparedStatement.setString(5, project.getStatus().getDisplayName());
            preparedStatement.setTimestamp(7, convertJavaDateToSqlDate(project.getDateBegin()));
            preparedStatement.setTimestamp(8, convertJavaDateToSqlDate(project.getDateEnd()));
            preparedStatement.setString(9, project.getId());
            preparedStatement.executeUpdate();
            return project;
        }
    }

}
