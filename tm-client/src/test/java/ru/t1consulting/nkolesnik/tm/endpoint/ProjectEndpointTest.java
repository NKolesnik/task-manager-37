package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.project.*;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.marker.SoapCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.User;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private static final String PROJECT_NAME = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final Status NEW_STATUS = Status.IN_PROGRESS;

    @NotNull
    private static final String NEW_PROJECT_NAME = "NEW_TEST_PROJECT_NAME";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "NEW_TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final String TEST_USER_LOGIN = "test";

    @NotNull
    private static final String TEST_USER_PASSWORD = "test";

    @NotNull
    private static final String ADMIN_USER_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_USER_PASSWORD = "admin";

    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();

    @Nullable
    private static final String NULL_TOKEN = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @NotNull
    private static final String BAD_PROJECT_ID = UUID.randomUUID().toString();

    @Nullable
    private static final Integer NULL_PROJECT_INDEX = null;

    @NotNull
    private static final Integer NEGATIVE_PROJECT_INDEX = -1;

    @NotNull
    private static final Integer BIG_PROJECT_INDEX = Integer.MAX_VALUE;

    @Nullable
    private static final String NULL_PROJECT_NAME = null;

    @Nullable
    private static final String NULL_PROJECT_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;
    private static final long COUNT_TEST_PROJECTS = 10L;
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();
    @Nullable
    private String testToken;
    @Nullable
    private String adminToken;
    @Nullable
    private String testUserId;
    @Nullable
    private String adminUserId;
    @Nullable
    private List<Project> testProjectList;

    @Before
    public void setup() {
        @NotNull final UserLoginRequest testLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final UserLoginResponse testLoginResponse = authEndpoint.login(testLoginRequest);
        testToken = testLoginResponse.getToken();
        @NotNull final UserProfileRequest testProfileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse testProfileResponse = authEndpoint.profile(testProfileRequest);
        @Nullable final User testUser = testProfileResponse.getUser();
        testUserId = testUser.getId();
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        @NotNull final UserLoginResponse adminLoginResponse = authEndpoint.login(adminLoginRequest);
        adminToken = adminLoginResponse.getToken();
        @NotNull final UserProfileRequest adminProfileRequest = new UserProfileRequest(adminToken);
        @NotNull final UserProfileResponse adminProfileResponse = authEndpoint.profile(adminProfileRequest);
        @Nullable final User adminUser = adminProfileResponse.getUser();
        adminUserId = adminUser.getId();
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(testToken);
        createManyTestProjects();
        testProjectList = projectEndpoint.listProject(listRequest).getProjects();
    }

    @After
    public void cleanup() {
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(testToken);
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest(adminToken);
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(testToken);
        projectEndpoint.clearProject(clearRequest);
        authEndpoint.logout(testLogoutRequest);
        authEndpoint.logout(adminLogoutRequest);
    }

    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(testToken, NULL_PROJECT_NAME, NULL_PROJECT_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(testToken, PROJECT_NAME, NULL_PROJECT_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(BAD_TOKEN, PROJECT_NAME, PROJECT_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(NULL_TOKEN, PROJECT_NAME, PROJECT_DESCRIPTION))
        );
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(testToken, PROJECT_NAME, PROJECT_DESCRIPTION);
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createProject(createRequest);
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        Assert.assertEquals(PROJECT_NAME, createResponse.getProject().getName());
        Assert.assertEquals(testUserId, createResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, createResponse.getProject().getUserId());
        @NotNull final String testProjectId = createResponse.getProject().getId();
        removeTestProject(testProjectId);
    }

    @Test
    public void reset() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(testToken);
        projectEndpoint.clearProject(clearRequest);
    }

    @Test
    public void listProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest(BAD_TOKEN)));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(testToken);
        @NotNull final ProjectListResponse listResponse = projectEndpoint.listProject(listRequest);
        Assert.assertNotNull(listResponse);
        Assert.assertNotNull(listResponse.getProjects());
        Assert.assertEquals(COUNT_TEST_PROJECTS, listResponse.getProjects().size());
        @NotNull final Project Project = listResponse.getProjects().get(0);
        Assert.assertEquals(testUserId, Project.getUserId());
        Assert.assertNotEquals(adminUserId, Project.getUserId());
    }

    @Test
    public void getProjectById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(testToken, NULL_PROJECT_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(testToken, BAD_PROJECT_ID))
        );
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        @NotNull final String ProjectId = Project.getId();
        @NotNull final ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(testToken, ProjectId);
        @NotNull final ProjectGetByIdResponse getByIdResponse = projectEndpoint.getProjectById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse);
        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(testUserId, getByIdResponse.getProject().getUserId());
        Assert.assertEquals(PROJECT_NAME, getByIdResponse.getProject().getName());
        Assert.assertNotEquals(adminUserId, getByIdResponse.getProject().getUserId());
    }

    @Test
    public void getProjectByIndex() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(testToken, NULL_PROJECT_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(testToken, NEGATIVE_PROJECT_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(testToken, BIG_PROJECT_INDEX))
        );
        @NotNull final ProjectGetByIndexRequest getByIndexRequest = new ProjectGetByIndexRequest(testToken, 1);
        @NotNull final ProjectGetByIndexResponse getByIndexResponse = projectEndpoint.getProjectByIndex(getByIndexRequest);
        Assert.assertNotNull(getByIndexResponse);
        Assert.assertNotNull(getByIndexResponse.getProject());
        Assert.assertEquals(PROJECT_NAME, getByIndexResponse.getProject().getName());
        Assert.assertEquals(testUserId, getByIndexResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, getByIndexResponse.getProject().getUserId());
    }

    @Test
    public void updateProjectById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest()));
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        @NotNull final String oldProjectName = Project.getName();
        @NotNull final String oldProjectDescription = Project.getDescription();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(testToken, NULL_PROJECT_ID, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(testToken, BAD_PROJECT_ID, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(testToken, Project.getId(), NULL_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(testToken, Project.getId(), NEW_PROJECT_NAME, NULL_PROJECT_DESCRIPTION)
                )
        );
        @NotNull final ProjectUpdateByIdRequest updateByIdRequest =
                new ProjectUpdateByIdRequest(testToken, Project.getId(), NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectUpdateByIdResponse ProjectUpdateByIdResponse = projectEndpoint.updateProjectById(updateByIdRequest);
        Assert.assertNotNull(ProjectUpdateByIdResponse);
        Assert.assertNotNull(ProjectUpdateByIdResponse.getProject());
        Assert.assertEquals(NEW_PROJECT_NAME, ProjectUpdateByIdResponse.getProject().getName());
        Assert.assertEquals(NEW_PROJECT_DESCRIPTION, ProjectUpdateByIdResponse.getProject().getDescription());
        Assert.assertNotEquals(oldProjectName, ProjectUpdateByIdResponse.getProject().getName());
        Assert.assertNotEquals(oldProjectDescription, ProjectUpdateByIdResponse.getProject().getDescription());
        Assert.assertEquals(testUserId, ProjectUpdateByIdResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, ProjectUpdateByIdResponse.getProject().getUserId());
    }

    @Test
    public void updateProjectByIndex() {
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        @NotNull final String oldProjectName = Project.getName();
        @NotNull final String oldProjectDescription = Project.getDescription();
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(testToken, NULL_PROJECT_INDEX, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(testToken, NEGATIVE_PROJECT_INDEX, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(testToken, BIG_PROJECT_INDEX, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(testToken, 1, NULL_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(testToken, 1, NEW_PROJECT_NAME, NULL_PROJECT_DESCRIPTION)
                )
        );
        @NotNull final ProjectUpdateByIndexRequest updateByIndexRequest =
                new ProjectUpdateByIndexRequest(testToken, 1, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectUpdateByIndexResponse ProjectUpdateByIndexResponse =
                projectEndpoint.updateProjectByIndex(updateByIndexRequest);
        Assert.assertNotNull(ProjectUpdateByIndexResponse);
        Assert.assertNotNull(ProjectUpdateByIndexResponse.getProject());
        Assert.assertEquals(NEW_PROJECT_NAME, ProjectUpdateByIndexResponse.getProject().getName());
        Assert.assertEquals(NEW_PROJECT_DESCRIPTION, ProjectUpdateByIndexResponse.getProject().getDescription());
        Assert.assertNotEquals(oldProjectName, ProjectUpdateByIndexResponse.getProject().getName());
        Assert.assertNotEquals(oldProjectDescription, ProjectUpdateByIndexResponse.getProject().getDescription());
        Assert.assertEquals(testUserId, ProjectUpdateByIndexResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, ProjectUpdateByIndexResponse.getProject().getUserId());
    }

    @Test
    public void startProjectById() {
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest(testToken, NULL_PROJECT_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest(testToken, BAD_PROJECT_ID))
        );
        @NotNull final ProjectStartByIdRequest startByIdRequest = new ProjectStartByIdRequest(testToken, Project.getId());
        @NotNull final ProjectStartByIdResponse startByIdResponse = projectEndpoint.startProjectById(startByIdRequest);
        Assert.assertNotNull(startByIdResponse);
        Assert.assertNotNull(startByIdResponse.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, startByIdResponse.getProject().getStatus());
        Assert.assertEquals(testUserId, startByIdResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, startByIdResponse.getProject().getUserId());
    }

    @Test
    public void startProjectByIndex() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(testToken, NULL_PROJECT_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(testToken, BIG_PROJECT_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(testToken, NEGATIVE_PROJECT_INDEX))
        );
        @NotNull final ProjectStartByIndexRequest startByIndexRequest = new ProjectStartByIndexRequest(testToken, 1);
        @NotNull final ProjectStartByIndexResponse startByIndexResponse =
                projectEndpoint.startProjectByIndex(startByIndexRequest);
        Assert.assertNotNull(startByIndexResponse);
        Assert.assertNotNull(startByIndexResponse.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, startByIndexResponse.getProject().getStatus());
        Assert.assertEquals(testUserId, startByIndexResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, startByIndexResponse.getProject().getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(testToken, NULL_PROJECT_ID, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(testToken, BAD_PROJECT_ID, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(testToken, Project.getId(), NULL_STATUS)
                )
        );
        @NotNull final ProjectChangeStatusByIdRequest changeStatusByIdRequest =
                new ProjectChangeStatusByIdRequest(testToken, Project.getId(), NEW_STATUS);
        @NotNull final ProjectChangeStatusByIdResponse changeProjectStatusByIdResponse =
                projectEndpoint.changeProjectStatusById(changeStatusByIdRequest);
        Assert.assertNotNull(changeProjectStatusByIdResponse);
        Assert.assertNotNull(changeProjectStatusByIdResponse.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, changeProjectStatusByIdResponse.getProject().getStatus());
        Assert.assertEquals(testUserId, changeProjectStatusByIdResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, changeProjectStatusByIdResponse.getProject().getUserId());
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(testToken, NULL_PROJECT_INDEX, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(testToken, BIG_PROJECT_INDEX, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(testToken, NEGATIVE_PROJECT_INDEX, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(testToken, 1, NULL_STATUS)
                )
        );
        @NotNull final ProjectChangeStatusByIndexRequest changeStatusByIndexRequest =
                new ProjectChangeStatusByIndexRequest(testToken, 1, NEW_STATUS);
        @NotNull final ProjectChangeStatusByIndexResponse changeStatusByIndexResponse =
                projectEndpoint.changeProjectStatusByIndex(changeStatusByIndexRequest);
        Assert.assertNotNull(changeStatusByIndexResponse);
        Assert.assertNotNull(changeStatusByIndexResponse.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, changeStatusByIndexResponse.getProject().getStatus());
        Assert.assertEquals(testUserId, changeStatusByIndexResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, changeStatusByIndexResponse.getProject().getUserId());
    }

    @Test
    public void completeProjectStatusById() {
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(testToken, NULL_PROJECT_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(testToken, BAD_PROJECT_ID))
        );
        @NotNull final ProjectCompleteByIdRequest completeByIdRequest =
                new ProjectCompleteByIdRequest(testToken, Project.getId());
        @NotNull final ProjectCompleteByIdResponse completeByIdResponse =
                projectEndpoint.completeProjectStatusById(completeByIdRequest);
        Assert.assertNotNull(completeByIdResponse);
        Assert.assertNotNull(completeByIdResponse.getProject());
        Assert.assertEquals(Status.COMPLETED, completeByIdResponse.getProject().getStatus());
        Assert.assertEquals(testUserId, completeByIdResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, completeByIdResponse.getProject().getUserId());
    }

    @Test
    public void completeProjectStatusByIndex() {
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(new ProjectCompleteByIndexRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(new ProjectCompleteByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(new ProjectCompleteByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(new ProjectCompleteByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(
                        new ProjectCompleteByIndexRequest(testToken, NULL_PROJECT_INDEX)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(
                        new ProjectCompleteByIndexRequest(testToken, BIG_PROJECT_INDEX)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.completeProjectStatusByIndex(
                        new ProjectCompleteByIndexRequest(testToken, NEGATIVE_PROJECT_INDEX)
                )
        );
        @NotNull final ProjectCompleteByIndexRequest completeByIndexRequest =
                new ProjectCompleteByIndexRequest(testToken, 1);
        @NotNull final ProjectCompleteByIndexResponse completeByIndexResponse =
                projectEndpoint.completeProjectStatusByIndex(completeByIndexRequest);
        Assert.assertNotNull(completeByIndexResponse);
        Assert.assertNotNull(completeByIndexResponse.getProject());
        Assert.assertEquals(Status.COMPLETED, completeByIndexResponse.getProject().getStatus());
        Assert.assertEquals(testUserId, completeByIndexResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, completeByIndexResponse.getProject().getUserId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(testToken, NULL_PROJECT_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(testToken, BAD_PROJECT_ID))
        );
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(testToken, Project.getId());
        @NotNull final ProjectRemoveByIdResponse removeByIdResponse = projectEndpoint.removeProjectById(removeByIdRequest);
        Assert.assertNotNull(removeByIdResponse);
        Assert.assertNotNull(removeByIdResponse.getProject());
        Assert.assertEquals(Project.getId(), removeByIdResponse.getProject().getId());
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(testToken, Project.getId()))
        );
        Assert.assertEquals(testUserId, removeByIdResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, removeByIdResponse.getProject().getUserId());
    }

    @Test
    public void removeProjectByIndex() {
        Assert.assertNotNull(testProjectList);
        @NotNull final Project Project = testProjectList.get(0);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(testToken, NULL_PROJECT_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(testToken, BIG_PROJECT_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(testToken, NEGATIVE_PROJECT_INDEX))
        );
        @NotNull final ProjectRemoveByIndexRequest removeByIndexRequest = new ProjectRemoveByIndexRequest(testToken, 1);
        @NotNull final ProjectRemoveByIndexResponse removeByIndexResponse =
                projectEndpoint.removeProjectByIndex(removeByIndexRequest);
        Assert.assertNotNull(removeByIndexResponse);
        Assert.assertNotNull(removeByIndexResponse.getProject());
        Assert.assertEquals(Project.getId(), removeByIndexResponse.getProject().getId());
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(new ProjectGetByIdRequest(testToken, Project.getId()))
        );
        Assert.assertEquals(testUserId, removeByIndexResponse.getProject().getUserId());
        Assert.assertNotEquals(adminUserId, removeByIndexResponse.getProject().getUserId());
    }

    private void createManyTestProjects() {
        for (int i = 0; i < COUNT_TEST_PROJECTS; i++) {
            @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(testToken);
            createRequest.setName(PROJECT_NAME);
            createRequest.setDescription(PROJECT_DESCRIPTION);
            projectEndpoint.createProject(createRequest);
        }
    }


    private void removeTestProject(@NotNull final String testProjectId) {
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(testToken);
        removeByIdRequest.setId(testProjectId);
        projectEndpoint.removeProjectById(removeByIdRequest);
    }

}
