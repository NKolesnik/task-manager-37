package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectCreateRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.*;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.ProjectCreateResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.marker.SoapCategory;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final String TASK_NAME = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final Status NEW_STATUS = Status.IN_PROGRESS;

    @NotNull
    private static final String NEW_TASK_NAME = "NEW_TEST_TASK_NAME";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "NEW_TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String TEST_USER_LOGIN = "test";

    @NotNull
    private static final String TEST_USER_PASSWORD = "test";

    @NotNull
    private static final String ADMIN_USER_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_USER_PASSWORD = "admin";

    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();

    @Nullable
    private static final String NULL_TOKEN = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @NotNull
    private static final String BAD_TASK_ID = UUID.randomUUID().toString();

    @Nullable
    private static final Integer NULL_TASK_INDEX = null;

    @NotNull
    private static final Integer NEGATIVE_TASK_INDEX = -1;

    @NotNull
    private static final Integer BIG_TASK_INDEX = Integer.MAX_VALUE;

    @Nullable
    private static final String NULL_TASK_NAME = null;

    @Nullable
    private static final String NULL_TASK_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;
    private static final long COUNT_TEST_TASKS = 10L;
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();
    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance();
    @Nullable
    private String testToken;
    @Nullable
    private String adminToken;
    @Nullable
    private String testUserId;
    @Nullable
    private String adminUserId;
    @Nullable
    private String testProjectId;
    @Nullable
    private List<Task> testTasksList;

    @Before
    public void setup() {
        @NotNull final UserLoginRequest testLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final UserLoginResponse testLoginResponse = authEndpoint.login(testLoginRequest);
        testToken = testLoginResponse.getToken();
        @NotNull final UserProfileRequest testProfileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse testProfileResponse = authEndpoint.profile(testProfileRequest);
        @Nullable final User testUser = testProfileResponse.getUser();
        testUserId = testUser.getId();
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        @NotNull final UserLoginResponse adminLoginResponse = authEndpoint.login(adminLoginRequest);
        adminToken = adminLoginResponse.getToken();
        @NotNull final UserProfileRequest adminProfileRequest = new UserProfileRequest(adminToken);
        @NotNull final UserProfileResponse adminProfileResponse = authEndpoint.profile(adminProfileRequest);
        @Nullable final User adminUser = adminProfileResponse.getUser();
        adminUserId = adminUser.getId();
        @NotNull final TaskListRequest listRequest = new TaskListRequest(testToken);
        createManyTestTasks();
        testProjectId = createTestProject();
        testTasksList = taskEndpoint.listTask(listRequest).getTasks();
        bindTasksToProject();
    }

    @After
    public void cleanup() {
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(testToken);
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest(adminToken);
        @NotNull final TaskClearRequest clearRequest = new TaskClearRequest(testToken);
        taskEndpoint.clearTask(clearRequest);
        removeTestProject(testProjectId);
        authEndpoint.logout(testLogoutRequest);
        authEndpoint.logout(adminLogoutRequest);
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(testToken, NULL_TASK_NAME, NULL_TASK_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(testToken, TASK_NAME, NULL_TASK_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(BAD_TOKEN, TASK_NAME, TASK_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(NULL_TOKEN, TASK_NAME, TASK_DESCRIPTION))
        );
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken, TASK_NAME, TASK_DESCRIPTION);
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
        Assert.assertEquals(TASK_NAME, createResponse.getTask().getName());
        Assert.assertEquals(testUserId, createResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, createResponse.getTask().getUserId());
        @NotNull final String testTaskId = createResponse.getTask().getId();
        removeTestTask(testTaskId);
    }

    @Test
    public void reset() {
        @NotNull final TaskClearRequest clearRequest = new TaskClearRequest(testToken);
        taskEndpoint.clearTask(clearRequest);
    }

    @Test
    public void listTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest(BAD_TOKEN)));
        @NotNull final TaskListRequest listRequest = new TaskListRequest(testToken);
        @NotNull final TaskListResponse listResponse = taskEndpoint.listTask(listRequest);
        Assert.assertNotNull(listResponse);
        Assert.assertNotNull(listResponse.getTasks());
        Assert.assertEquals(COUNT_TEST_TASKS, listResponse.getTasks().size());
        @NotNull final Task task = listResponse.getTasks().get(0);
        Assert.assertEquals(testUserId, task.getUserId());
        Assert.assertNotEquals(adminUserId, task.getUserId());
    }

    @Test
    public void getTaskById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken, BAD_TASK_ID))
        );
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        @NotNull final String taskId = task.getId();
        @NotNull final TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(testToken, taskId);
        @NotNull final TaskGetByIdResponse getByIdResponse = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse);
        Assert.assertNotNull(getByIdResponse.getTask());
        Assert.assertEquals(testUserId, getByIdResponse.getTask().getUserId());
        Assert.assertEquals(TASK_NAME, getByIdResponse.getTask().getName());
        Assert.assertNotEquals(adminUserId, getByIdResponse.getTask().getUserId());
    }

    @Test
    public void getTaskByIndex() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(testToken, NULL_TASK_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(testToken, NEGATIVE_TASK_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByIndex(new TaskGetByIndexRequest(testToken, BIG_TASK_INDEX))
        );
        @NotNull final TaskGetByIndexRequest getByIndexRequest = new TaskGetByIndexRequest(testToken, 1);
        @NotNull final TaskGetByIndexResponse getByIndexResponse = taskEndpoint.getTaskByIndex(getByIndexRequest);
        Assert.assertNotNull(getByIndexResponse);
        Assert.assertNotNull(getByIndexResponse.getTask());
        Assert.assertEquals(TASK_NAME, getByIndexResponse.getTask().getName());
        Assert.assertEquals(testUserId, getByIndexResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, getByIndexResponse.getTask().getUserId());
    }

    @Test
    public void getTaskByProjectId() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskByProjectId(new TaskGetByProjectIdRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByProjectId(new TaskGetByProjectIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByProjectId(new TaskGetByProjectIdRequest(BAD_TOKEN))
        );
        @NotNull final TaskGetByProjectIdRequest getByProjectIdRequest =
                new TaskGetByProjectIdRequest(testToken, testProjectId);
        @NotNull final TaskGetByProjectIdResponse taskGetByProjectIdResponse =
                taskEndpoint.getTaskByProjectId(getByProjectIdRequest);
        Assert.assertNotNull(taskGetByProjectIdResponse);
        Assert.assertNotNull(taskGetByProjectIdResponse.getTasks());
        @NotNull final Task task = taskGetByProjectIdResponse.getTasks().get(0);
        Assert.assertEquals(testProjectId, task.getProjectId());
        Assert.assertEquals(testUserId, task.getUserId());
        Assert.assertNotEquals(adminUserId, task.getUserId());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest()));
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        @NotNull final String oldTaskName = task.getName();
        @NotNull final String oldTaskDescription = task.getDescription();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, NULL_TASK_ID, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, BAD_TASK_ID, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, task.getId(), NULL_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, task.getId(), NEW_TASK_NAME, NULL_TASK_DESCRIPTION)
                )
        );
        @NotNull final TaskUpdateByIdRequest updateByIdRequest =
                new TaskUpdateByIdRequest(testToken, task.getId(), NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(updateByIdRequest);
        Assert.assertNotNull(taskUpdateByIdResponse);
        Assert.assertNotNull(taskUpdateByIdResponse.getTask());
        Assert.assertEquals(NEW_TASK_NAME, taskUpdateByIdResponse.getTask().getName());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, taskUpdateByIdResponse.getTask().getDescription());
        Assert.assertNotEquals(oldTaskName, taskUpdateByIdResponse.getTask().getName());
        Assert.assertNotEquals(oldTaskDescription, taskUpdateByIdResponse.getTask().getDescription());
        Assert.assertEquals(testUserId, taskUpdateByIdResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, taskUpdateByIdResponse.getTask().getUserId());
    }

    @Test
    public void updateTaskByIndex() {
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        @NotNull final String oldTaskName = task.getName();
        @NotNull final String oldTaskDescription = task.getDescription();
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(testToken, NULL_TASK_INDEX, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(testToken, NEGATIVE_TASK_INDEX, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(testToken, BIG_TASK_INDEX, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(testToken, 1, NULL_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(testToken, 1, NEW_TASK_NAME, NULL_TASK_DESCRIPTION)
                )
        );
        @NotNull final TaskUpdateByIndexRequest updateByIndexRequest =
                new TaskUpdateByIndexRequest(testToken, 1, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        @NotNull final TaskUpdateByIndexResponse taskUpdateByIndexResponse =
                taskEndpoint.updateTaskByIndex(updateByIndexRequest);
        Assert.assertNotNull(taskUpdateByIndexResponse);
        Assert.assertNotNull(taskUpdateByIndexResponse.getTask());
        Assert.assertEquals(NEW_TASK_NAME, taskUpdateByIndexResponse.getTask().getName());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, taskUpdateByIndexResponse.getTask().getDescription());
        Assert.assertNotEquals(oldTaskName, taskUpdateByIndexResponse.getTask().getName());
        Assert.assertNotEquals(oldTaskDescription, taskUpdateByIndexResponse.getTask().getDescription());
        Assert.assertEquals(testUserId, taskUpdateByIndexResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, taskUpdateByIndexResponse.getTask().getUserId());
    }

    @Test
    public void startTaskById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(testToken, BAD_TASK_ID))
        );
        @NotNull final TaskStartByIdRequest startByIdRequest = new TaskStartByIdRequest(testToken, task.getId());
        @NotNull final TaskStartByIdResponse startByIdResponse = taskEndpoint.startTaskById(startByIdRequest);
        Assert.assertNotNull(startByIdResponse);
        Assert.assertNotNull(startByIdResponse.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, startByIdResponse.getTask().getStatus());
        Assert.assertEquals(testUserId, startByIdResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, startByIdResponse.getTask().getUserId());
    }

    @Test
    public void startTaskByIndex() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(testToken, NULL_TASK_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(testToken, BIG_TASK_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(testToken, NEGATIVE_TASK_INDEX))
        );
        @NotNull final TaskStartByIndexRequest startByIndexRequest = new TaskStartByIndexRequest(testToken, 1);
        @NotNull final TaskStartByIndexResponse startByIndexResponse =
                taskEndpoint.startTaskByIndex(startByIndexRequest);
        Assert.assertNotNull(startByIndexResponse);
        Assert.assertNotNull(startByIndexResponse.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, startByIndexResponse.getTask().getStatus());
        Assert.assertEquals(testUserId, startByIndexResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, startByIndexResponse.getTask().getUserId());
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(testToken, NULL_TASK_ID, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(testToken, BAD_TASK_ID, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(testToken, task.getId(), NULL_STATUS)
                )
        );
        @NotNull final TaskChangeStatusByIdRequest changeStatusByIdRequest =
                new TaskChangeStatusByIdRequest(testToken, task.getId(), NEW_STATUS);
        @NotNull final TaskChangeStatusByIdResponse changeTaskStatusByIdResponse =
                taskEndpoint.changeTaskStatusById(changeStatusByIdRequest);
        Assert.assertNotNull(changeTaskStatusByIdResponse);
        Assert.assertNotNull(changeTaskStatusByIdResponse.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, changeTaskStatusByIdResponse.getTask().getStatus());
        Assert.assertEquals(testUserId, changeTaskStatusByIdResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, changeTaskStatusByIdResponse.getTask().getUserId());
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(testToken, NULL_TASK_INDEX, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(testToken, BIG_TASK_INDEX, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(testToken, NEGATIVE_TASK_INDEX, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(testToken, 1, NULL_STATUS)
                )
        );
        @NotNull final TaskChangeStatusByIndexRequest changeStatusByIndexRequest =
                new TaskChangeStatusByIndexRequest(testToken, 1, NEW_STATUS);
        @NotNull final TaskChangeStatusByIndexResponse changeStatusByIndexResponse =
                taskEndpoint.changeTaskStatusByIndex(changeStatusByIndexRequest);
        Assert.assertNotNull(changeStatusByIndexResponse);
        Assert.assertNotNull(changeStatusByIndexResponse.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, changeStatusByIndexResponse.getTask().getStatus());
        Assert.assertEquals(testUserId, changeStatusByIndexResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, changeStatusByIndexResponse.getTask().getUserId());
    }

    @Test
    public void completeTaskStatusById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(testToken, BAD_TASK_ID))
        );
        @NotNull final TaskCompleteByIdRequest completeByIdRequest =
                new TaskCompleteByIdRequest(testToken, task.getId());
        @NotNull final TaskCompleteByIdResponse completeByIdResponse =
                taskEndpoint.completeTaskStatusById(completeByIdRequest);
        Assert.assertNotNull(completeByIdResponse);
        Assert.assertNotNull(completeByIdResponse.getTask());
        Assert.assertEquals(Status.COMPLETED, completeByIdResponse.getTask().getStatus());
        Assert.assertEquals(testUserId, completeByIdResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, completeByIdResponse.getTask().getUserId());
    }

    @Test
    public void completeTaskStatusByIndex() {
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(new TaskCompleteByIndexRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(new TaskCompleteByIndexRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(new TaskCompleteByIndexRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(new TaskCompleteByIndexRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(
                        new TaskCompleteByIndexRequest(testToken, NULL_TASK_INDEX)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(
                        new TaskCompleteByIndexRequest(testToken, BIG_TASK_INDEX)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusByIndex(
                        new TaskCompleteByIndexRequest(testToken, NEGATIVE_TASK_INDEX)
                )
        );
        @NotNull final TaskCompleteByIndexRequest completeByIndexRequest =
                new TaskCompleteByIndexRequest(testToken, 1);
        @NotNull final TaskCompleteByIndexResponse completeByIndexResponse =
                taskEndpoint.completeTaskStatusByIndex(completeByIndexRequest);
        Assert.assertNotNull(completeByIndexResponse);
        Assert.assertNotNull(completeByIndexResponse.getTask());
        Assert.assertEquals(Status.COMPLETED, completeByIndexResponse.getTask().getStatus());
        Assert.assertEquals(testUserId, completeByIndexResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, completeByIndexResponse.getTask().getUserId());
    }

    @Test
    public void removeTaskById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(testToken, BAD_TASK_ID))
        );
        @NotNull final TaskRemoveByIdRequest removeByIdRequest = new TaskRemoveByIdRequest(testToken, task.getId());
        @NotNull final TaskRemoveByIdResponse removeByIdResponse = taskEndpoint.removeTaskById(removeByIdRequest);
        Assert.assertNotNull(removeByIdResponse);
        Assert.assertNotNull(removeByIdResponse.getTask());
        Assert.assertEquals(task.getId(), removeByIdResponse.getTask().getId());
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken, task.getId()))
        );
        Assert.assertEquals(testUserId, removeByIdResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, removeByIdResponse.getTask().getUserId());
    }

    @Test
    public void removeTaskByIndex() {
        Assert.assertNotNull(testTasksList);
        @NotNull final Task task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(testToken, NULL_TASK_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(testToken, BIG_TASK_INDEX))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(testToken, NEGATIVE_TASK_INDEX))
        );
        @NotNull final TaskRemoveByIndexRequest removeByIndexRequest = new TaskRemoveByIndexRequest(testToken, 1);
        @NotNull final TaskRemoveByIndexResponse removeByIndexResponse =
                taskEndpoint.removeTaskByIndex(removeByIndexRequest);
        Assert.assertNotNull(removeByIndexResponse);
        Assert.assertNotNull(removeByIndexResponse.getTask());
        Assert.assertEquals(task.getId(), removeByIndexResponse.getTask().getId());
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken, task.getId()))
        );
        Assert.assertEquals(testUserId, removeByIndexResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, removeByIndexResponse.getTask().getUserId());
    }

    private void createManyTestTasks() {
        for (int i = 0; i < COUNT_TEST_TASKS; i++) {
            @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken);
            createRequest.setName(TASK_NAME);
            createRequest.setDescription(TASK_DESCRIPTION);
            taskEndpoint.createTask(createRequest);
        }
    }

    private void bindTasksToProject() {
        if (testTasksList != null) {
            for (Task task : testTasksList) {
                @NotNull final TaskBindToProjectRequest bindToProjectRequest = new TaskBindToProjectRequest(testToken);
                bindToProjectRequest.setProjectId(testProjectId);
                bindToProjectRequest.setTaskId(task.getId());
                projectTaskEndpoint.bindTaskToProject(bindToProjectRequest);
            }
        }
    }

    private void removeTestTask(@NotNull final String testTaskId) {
        @NotNull final TaskRemoveByIdRequest removeByIdRequest = new TaskRemoveByIdRequest(testToken);
        removeByIdRequest.setId(testTaskId);
        taskEndpoint.removeTaskById(removeByIdRequest);
    }

    @NotNull
    private String createTestProject() {
        if (testProjectId == null) {
            @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
            projectCreateRequest.setName(PROJECT_NAME);
            projectCreateRequest.setDescription(PROJECT_DESCRIPTION);
            @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.
                    createProject(projectCreateRequest);
            return projectCreateResponse.getProject().getId();
        } else {
            return testProjectId;
        }
    }

    private void removeTestProject(@NotNull final String testProjectId) {
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(testToken);
        removeByIdRequest.setId(testProjectId);
        projectEndpoint.removeProjectById(removeByIdRequest);
    }
}
